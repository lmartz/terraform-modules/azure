provider "tls" {
    # Configuration options
}
provider "azurerm" {
    features {}
}
locals {
    general-conf = {

        worker_nodes_conf = {
            size = "Standard_F2"
            tags = {
                Role = "WorkerNode"
            }
        }
    }
}
module "unmanaged-k8s" {

    source = "../"

    #Global-config
    resource_prefix = "lmart"
    location = "eastus2"

    #Vm-config
    admin_user = "lmartinez"

    #Network-config
    vnet-cidr = ["10.0.0.0/16"]
    subnet-cidr = ["10.0.1.0/24"]

    #Compute Config
    master_node_size = "Standard_F2"

    nodes_def = {
        node01 = local.general-conf.worker_nodes_conf,
        node02 = local.general-conf.worker_nodes_conf,
        node03 = local.general-conf.worker_nodes_conf,
    }
    #Security-config
    nsg_rules = {}

    #k8s-config
    k8s_version = "1.23.1-00"

    tags = {
        "Deployed_By" = "Terraform"
        "Role" = "Lab"
        "App" = "K8s"
    }
}