###########
# Providers
###########
terraform {
    required_providers {
        azurerm = {
        source = "hashicorp/azurerm"
        version = "2.90.0"
        }
        tls = {
        source = "hashicorp/tls"
        version = "3.1.0"
        }
    }
}
###########
# Locals Matrix
###########
locals {
    global-conf = {
        prefix   = var.resource_prefix
        location = var.location
        admin_user = var.admin_user
    }
    tags = var.tags
    vm_conf = merge({
        master = {
            size = var.master_node_size
            tags = {
                Role = "ControlPlane"
            }
        }
    }, var.nodes_def)
    nsg_rules = merge(
        {        
            ssh = {
                name                       = "ssh"
                priority                   = 202
                direction                  = "Inbound"
                access                     = "Allow"
                protocol                   = "Tcp"
                source_port_range          = "*"
                destination_port_range     = "22"
                destination_address_prefix = "*"
            }
        },var.nsg_rules
    )
}
###########
# Misc Conf
###########
data "http" "my_public_ip" {
    url = "http://ipv4.icanhazip.com"
}
resource "random_id" "server" {
  byte_length = 2
}
###########
# Rsa Conf
###########
resource "tls_private_key" "k8s-private-key" {
    algorithm   = "RSA"
    rsa_bits = "2048"
}
resource "local_file" "private_key" {
    content         = tls_private_key.k8s-private-key.private_key_pem
    filename        = "${path.root}/ansible/rsa-${local.global-conf.prefix}-${random_id.server.dec}.pem"
    file_permission = "0600"
}
###########
# Rg Conf
###########
resource "azurerm_resource_group" "k8s-rg" {
    name     = "rg-${local.global-conf.prefix}-${random_id.server.dec}"
    location = local.global-conf.location
}
###########
# Network Conf
###########
resource "azurerm_public_ip" "vm-pip" {
    for_each = local.vm_conf
    name                = "pip-${local.global-conf.prefix}-${each.key}-${random_id.server.dec}"
    location            = local.global-conf.location
    resource_group_name = azurerm_resource_group.k8s-rg.name
    allocation_method = "Dynamic"
    sku = "Basic"
}
resource "azurerm_virtual_network" "k8s-vnet" {
    name                = "vnet-${local.global-conf.prefix}-${random_id.server.dec}"
    address_space       = var.vnet-cidr
    location            = local.global-conf.location
    resource_group_name = azurerm_resource_group.k8s-rg.name
}
resource "azurerm_subnet" "k8s-subnet" {
    name                 = "subnet-${local.global-conf.prefix}-${random_id.server.dec}"
    resource_group_name  = azurerm_resource_group.k8s-rg.name
    virtual_network_name = azurerm_virtual_network.k8s-vnet.name
    address_prefixes     = var.subnet-cidr
}
###########
# VM Conf
###########
resource "azurerm_network_interface" "k8s-nic" {
    for_each = local.vm_conf
    name                = "nic-${local.global-conf.prefix}-${each.key}-${random_id.server.dec}"
    location            = local.global-conf.location
    resource_group_name = azurerm_resource_group.k8s-rg.name

    ip_configuration {
        name                          = "ipconf"
        subnet_id                     = azurerm_subnet.k8s-subnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = azurerm_public_ip.vm-pip[each.key].id
    }
}
resource "azurerm_linux_virtual_machine" "k8s-vm" {
    for_each = local.vm_conf
    name                = "vm-${local.global-conf.prefix}-${each.key}-${random_id.server.dec}"
    resource_group_name = azurerm_resource_group.k8s-rg.name
    location            = local.global-conf.location
    size                = each.value.size
    admin_username      = local.global-conf.admin_user
    allow_extension_operations = false
    network_interface_ids = [
        azurerm_network_interface.k8s-nic[each.key].id,
    ]

    admin_ssh_key {
        username   = local.global-conf.admin_user
        public_key = tls_private_key.k8s-private-key.public_key_openssh
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

        source_image_reference {
            publisher = "Canonical"
            offer     = "0001-com-ubuntu-server-focal"
            sku       = "20_04-lts"
            version   = "20.04.202201260"
        }

    tags = merge(local.tags, each.value.tags )
}

###########
# NSG Conf
###########
resource "azurerm_network_security_group" "k8s-nsg" {
    name                = "nsg-${local.global-conf.prefix}-${random_id.server.dec}"
    location            = local.global-conf.location
    resource_group_name = azurerm_resource_group.k8s-rg.name
    tags = local.tags
}
resource "azurerm_network_security_rule" "nsg-rules" {
    for_each                    = local.nsg_rules
    name                        = each.value.name
    direction                   = each.value.direction
    access                      = each.value.access
    priority                    = each.value.priority
    protocol                    = each.value.protocol
    source_port_range           = each.value.source_port_range
    destination_port_range      = each.value.destination_port_range
    source_address_prefix       = trim(data.http.my_public_ip.body, " \n")
    destination_address_prefix  = each.value.destination_address_prefix
    resource_group_name         = azurerm_resource_group.k8s-rg.name
    network_security_group_name = azurerm_network_security_group.k8s-nsg.name
}
resource "azurerm_subnet_network_security_group_association" "nsg-asso-k8s" {
    subnet_id                 = azurerm_subnet.k8s-subnet.id
    network_security_group_id = azurerm_network_security_group.k8s-nsg.id
}
###########
# Ansible Conf
###########
resource "local_file" "inventory" {
    # for_each = local.vm_conf
    content = templatefile("${path.module}/templates/inventory.tpl",
        {
        ansible_user = local.global-conf.admin_user
        ansible_rsa = basename(local_file.private_key.filename)
        master_ip = element([for vm in azurerm_linux_virtual_machine.k8s-vm : vm.public_ip_address if length(regexall("master", vm.name)) > 0],0)
        nodes_ansible_ip  = {for key, vm in azurerm_linux_virtual_machine.k8s-vm :  key => vm.public_ip_address if length(regexall("node", vm.name))>0}
        }
    )
    filename = "${path.root}/ansible/inventory"
}
resource "local_file" "ansible_cfg" {
    depends_on = [
        local_file.inventory
    ]
    # for_each = local.vm_conf
    content = templatefile("${path.module}/templates/ansible.cfg.tpl",
        {
            inventory = "inventory"
            remote_user = local.global-conf.admin_user
        }
    )
    filename = "${path.root}/ansible/ansible.cfg"
}
resource "local_file" "ansible_variables" {
    depends_on = [
        local_file.ansible_cfg
    ]
    # for_each = local.vm_conf
    content = templatefile("${path.module}/templates/ansible_variables.yml.tpl",
        {
            k8s_version = var.k8s_version
            admin_user = local.global-conf.admin_user
        }
    )
    filename = "${path.module}/ansible/ansible_variables.yml"
}
resource "null_resource" "ansible-run" {
    depends_on = [
        local_file.ansible_variables,
        local_file.ansible_cfg,
        local_file.inventory,
        azurerm_linux_virtual_machine.k8s-vm,
    ]
    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        # Bootstrap script called with private_ip of each node in the clutser
        command = "ansible-playbook ../${path.module}/ansible/playbooks/setup_cluster.yml"
        working_dir = "${path.root}/ansible"
    }
}