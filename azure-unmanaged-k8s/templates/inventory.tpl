[k8s_master]
master ansible_host=${master_ip} 

[k8s_nodes_hosts]
%{ for host, ip in nodes_ansible_ip ~}
${host} ansible_host=${ip}
%{ endfor ~}

[empty_group]

[cluster:children]
k8s_master
k8s_nodes_hosts

[cluster:vars]
ansible_connection=ssh
ansible_port=22
ansible_user=${ansible_user} 
ansible_ssh_private_key_file=${ansible_rsa}