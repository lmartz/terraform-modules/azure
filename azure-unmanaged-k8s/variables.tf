variable "resource_prefix" {
}
variable "location" {
}
variable "admin_user" {
}
variable "tags" {
}
variable "nsg_rules" {
    default = {}
}
variable "vnet-cidr" {
    type = list(string)
    default = [ "10.0.0.0/16" ]
}
variable "subnet-cidr" {
    type = list(string)
    default = [ "10.0.1.0/24" ]
}
variable "nodes_def" {
    type = map(object({
            size = string,
            tags = map(string)
    }))
}
variable "k8s_version" {
    type = string
    default = "1.23.1-00"
}
variable "master_node_size" {
    type = string
}