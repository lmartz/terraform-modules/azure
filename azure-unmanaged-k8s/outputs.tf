output "public_ip" {
  value = {
    for k, pip in azurerm_linux_virtual_machine.k8s-vm : k => pip.public_ip_address   
  }
}