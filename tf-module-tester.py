import os
from pathlib import Path
import subprocess
import sys
from dataclasses import dataclass
from typing import ClassVar

@dataclass
class TerraTest():
    go_timeout: ClassVar[str] ="30m"
    go_test_command: ClassVar[str] ='go test'
    go_args: ClassVar[str] = f"-timeout {go_timeout} -v"

    def get_diffs(self, remote_branch: str) -> list:
        """Get all the modified files in git between local repo and the specified remote branch

        Args:
            remote_branch (str): The git ref representing a remote branch/tag/commit

        Returns:
            list: A list containing all the path that contain a 'test' directory
        """
        diff_command=f'git diff --name-only $(git rev-parse {remote_branch}) | cut -d"/" -f1 | uniq'
        git_diff=os.popen(diff_command).read().splitlines()
        return [ directory for directory in git_diff if (directory and (Path().cwd() / directory / "test" ).is_dir()) ]

    def cmd_run(self, cmd: str) -> None:
        """The the specified command and wait for its completion.

        Args:
            cmd (str): The command to execute.
        """
        process = subprocess.Popen(cmd, shell=True)
        process.wait()
        if process.poll() != 0:
            sys.exit(1)


    # Running terratest command in each modified dir
    def run_tests(self) -> None:
        """Run all detected go test files for each module path that exists in the list of git diff
        """
        for module in self.get_diffs("origin/main"):
            try:
                module_path = Path(module)
                os.chdir(module_path)
                test_files = [ t_file for t_file in module_path.cwd().glob("test/*.go")]
                for test_file in test_files:
                    print(f"Terratest will run: {self.go_test_command} {self.go_args} over {test_file}")
                    self.cmd_run(f"{self.go_test_command} {self.go_args} {test_file}")
                os.chdir(module_path.resolve().parent)
            except Exception as e:
                print(e)

if __name__ == "__main__":
    tf_test = TerraTest()
    tf_test.run_tests()